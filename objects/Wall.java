package ija.homework1.objects;

import ija.homework1.tape.*;

public class Wall extends TapeObject {
	public Wall(String name) {
		super(name);
	}

	public boolean open() {
		return false;
	}

	public boolean canSeize() {
		return false;
	}
}