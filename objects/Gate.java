package ija.homework1.objects;

import ija.homework1.tape.*;

public class Gate extends TapeObject {
	private boolean opened = false;

	public Gate(String name) {
		super(name);
	}

	public boolean open() {
		if(!opened) {
			this.opened = true;
			return true;
		}
		else {
			return false;
		}
	}

	public boolean canSeize() {
		return opened;
	}
}