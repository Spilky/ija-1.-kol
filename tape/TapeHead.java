package ija.homework1.tape;

public class TapeHead {

	private int id;

	public TapeHead(int id) {
		this.id = id;
	}

	public int id() {
		return id;
	}

	public boolean equals(TapeHead head) {
		return this.id == head.id;
	}

	public int hashCode() {
		return this.id;
	}

}