package ija.homework1.tape;

public class TapeField {

	private int p;
	private TapeObject obj;
	private TapeHead head;

	public TapeField(int p) {
		this.p = p;
	}

	public TapeField(int p, TapeObject obj) {
		this.p = p;
		this.obj = obj;
	}

	public int position() {
		return p;
	}

	public boolean seize(TapeHead head) {
		if(canSeize()) {
			this.head = head;
			return true;
		}
		else {
			return false;
		}
	}

	public TapeHead leave() {
		TapeHead tmp_head = this.head;
		this.head = null;
		return tmp_head;
	}

	public boolean canSeize() {
		if(head != null) {
			return false;
		}
		if(obj == null) {
			return true;
		}
		return obj.canSeize();
	}

	public boolean open() {
		if(obj == null) {
			return false;
		}
		return obj.open();
	}

	public boolean equals(TapeField field) {
		return this.p == field.p && this.obj == field.obj && this.head == field.head;
	}

	public int hashCode() {
		return this.p;
	}

}