package ija.homework1.tape;

import ija.homework1.objects.*;

public abstract class TapeObject {

	protected String name;

	public TapeObject(String name) {
		this.name = name;
	}

	public abstract boolean canSeize();

	public abstract boolean open();

	public boolean equals(TapeObject obj) {
		if(obj instanceof Wall)
			return name.equals(obj.name);

		return (name.equals(obj.name)) && ((Gate)this).canSeize() == ((Gate)obj).canSeize();
	}

	public int hashCode() {
		return name.length();
	}

}
